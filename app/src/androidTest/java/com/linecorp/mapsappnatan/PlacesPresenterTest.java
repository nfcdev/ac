package com.linecorp.mapsappnatan;

import android.support.test.annotation.UiThreadTest;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.maps.model.LatLng;
import com.linecorp.mapsappnatan.connections.RestApiKey;
import com.linecorp.mapsappnatan.model.AddressResponse;
import com.linecorp.mapsappnatan.presenter.PlacesContract;
import com.linecorp.mapsappnatan.presenter.PlacesPresenter;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;

/**
 * Created by natanbrito on 18/06/2018.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class PlacesPresenterTest extends TestCase {


    PlacesContract.View view;
    LatLng latLng;
    List<LatLng> latLngList = new ArrayList<>();
    List<AddressResponse.ResultsBean> listAddress = new ArrayList<>();
    RestApiKey task;
    String place;
    String key;
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private MainActivity mainActivity;

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();

    }

    @Test
    public void testPlaceEmptyValue() {
        place = "";
        onView(withId(R.id.et_place)).perform(typeText(place), closeSoftKeyboard());
        onView(withId(R.id.bt_search)).perform(click());
        assertNotNull("");
    }

    @Test
    public void testPlaceWithValue() throws Throwable {
        place = "Sydney";
        onView(withId(R.id.et_place)).perform(typeText(place), closeSoftKeyboard());
        onView(withId(R.id.bt_search)).perform(click());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.placesPresenter.placesValidator(place);
            }
        });
        assertNotNull(place);
    }

    @Test
    public void testUnknowPlace() throws Throwable {
        place = "jdhjdhajdh";
        onView(withId(R.id.et_place)).perform(typeText(place), closeSoftKeyboard());
        onView(withId(R.id.bt_search)).perform(click());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainActivity.placesPresenter.placesValidator(place);
            }
        });
        assertNotNull(place);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }
}
