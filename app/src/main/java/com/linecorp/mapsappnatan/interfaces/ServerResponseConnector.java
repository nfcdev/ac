package com.linecorp.mapsappnatan.interfaces;

public interface ServerResponseConnector {

    void onConnectionResult(int statusCode, Object responseData);
}
