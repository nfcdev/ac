package com.linecorp.mapsappnatan.data;

import android.provider.BaseColumns;

/**
 * Created by natanbrito on 17/06/2018.
 */

public class PlacesFields {

    private PlacesFields(){}

    public static final class PlacesEntry implements BaseColumns {
        public static final String TABLE_NAME = "places_list";
        public static final String PLACE_COLUMN = "place";
        public static final String LAT_COLUMN = "lat";
        public static final String LNG_COLUMN = "lng";

    }


}
