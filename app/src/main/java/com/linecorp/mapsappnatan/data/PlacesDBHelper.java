package com.linecorp.mapsappnatan.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.linecorp.mapsappnatan.data.PlacesFields.*;
/**
 * Created by natanbrito on 17/06/2018.
 */

public class PlacesDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "placeslist.db";
    public static final int DATABASE_VERSION= 1;

    public PlacesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            final String SQL_CREATE_PLACESLIST_DB = "CREATE TABLE "+ PlacesEntry.TABLE_NAME+
                    " ("+PlacesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                    PlacesEntry.PLACE_COLUMN + " TEXT NOT NULL UNIQUE, "+
                    PlacesEntry.LAT_COLUMN + " DOUBLE NOT NULL,"+
                    PlacesEntry.LNG_COLUMN + " DOUBLE NOT NULL);";

            db.execSQL(SQL_CREATE_PLACESLIST_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + PlacesEntry.TABLE_NAME);
            onCreate(db);
    }
}
