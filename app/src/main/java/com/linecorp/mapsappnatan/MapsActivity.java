package com.linecorp.mapsappnatan;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.linecorp.mapsappnatan.data.PlacesDBHelper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.linecorp.mapsappnatan.presenter.MapsPresenter;
import com.linecorp.mapsappnatan.presenter.PlacesContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,PlacesContract.MapsView {

    private GoogleMap mMap;
    private List<LatLng> latLngList = new ArrayList<>();
    private boolean isSaved = false;
    private AlertDialog.Builder builder;
    MenuItem item;
    String place;
    @BindView(R.id.cl)
    ConstraintLayout cl;
    Context context;
    PlacesContract.MapsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        PlacesDBHelper dbHelper = new PlacesDBHelper(this);
        ButterKnife.bind(this);
        context = this;

        presenter = new MapsPresenter(this,dbHelper);
        Intent intent = getIntent();
        latLngList = intent.getParcelableArrayListExtra("latLngList");
        place = intent.getStringExtra("place");
        presenter.startDb();
    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        createMarkers(latLngList);

    }

    private void createMarkers(List<LatLng> latLngList) {
        MarkerOptions markerOptions = new MarkerOptions();
        if(mMap != null) {
            mMap.clear();
            for (LatLng coordinate : latLngList) {
                markerOptions.position(coordinate)
                .title(place)
                .snippet(coordinate.latitude+"," +coordinate.longitude);
                mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(coordinate));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(2));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.maps_menu,menu);

        item = menu.findItem(R.id.item_action);
        isSaved = presenter.isPlaceSaved(place);
        if(isSaved)
            item.setIcon(android.R.drawable.ic_menu_delete);
        else
            item.setIcon(android.R.drawable.ic_menu_save);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        builder = new AlertDialog.Builder(this);

        if(isSaved) {
            builder.setTitle(getResources().getString(R.string.remove_place))
                    .setMessage(getResources().getString(R.string.remove_place_question))
                    .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            presenter.removePlace();
                            isSaved = false;
                            item.setIcon(android.R.drawable.ic_menu_save);
                            Log.d("TESTE", "onClick: "+presenter.getSavedPlaces().getCount());
                            Snackbar.make(cl,getResources().getString(R.string.msg_removed_place),Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getResources().getString(R.string.ok), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    }).show();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel),null)
                    .show();


        }else {
            presenter.savePlace(place, latLngList);
            isSaved = true;
            item.setIcon(android.R.drawable.ic_menu_delete);
            Log.d("TESTE", "onClick: "+ presenter.getSavedPlaces().getCount());
            String messageOk = getResources().getString(R.string.ok);
            Snackbar.make(cl,getResources().getString(R.string.msg_added_place),Snackbar.LENGTH_INDEFINITE)
                    .setAction(getResources().getString(R.string.ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            }).show();
        }


        return super.onOptionsItemSelected(item);
    }


}
