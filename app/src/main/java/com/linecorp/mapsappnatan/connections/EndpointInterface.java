package com.linecorp.mapsappnatan.connections;

import com.linecorp.mapsappnatan.model.AddressResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by natanbrito on 16/06/2018.
 */

public interface EndpointInterface {

    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/geocode/";


    @GET("json")
    Call<AddressResponse> listAddress(@Query("address") String address,@Query("key") String key);

}
