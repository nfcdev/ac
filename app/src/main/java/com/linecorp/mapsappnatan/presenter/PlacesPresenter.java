package com.linecorp.mapsappnatan.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.linecorp.mapsappnatan.MapsActivity;
import com.linecorp.mapsappnatan.connections.RestApiKey;
import com.linecorp.mapsappnatan.interfaces.ServerResponseConnector;
import com.linecorp.mapsappnatan.model.AddressResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by natanbrito on 17/06/2018.
 */

public class PlacesPresenter implements PlacesContract.Presenter, ServerResponseConnector {

    PlacesContract.View view;
    LatLng latLng;
    List<LatLng> latLngList = new ArrayList<>();
    List<AddressResponse.ResultsBean> listAddress = new ArrayList<>();
    RestApiKey task;
    String place;
    String key;

    public PlacesPresenter(PlacesContract.View view, String key) {
        this.view = view;
        this.key = key;
    }

    public void placesValidator(String place){
        if(place != null && !place.isEmpty()){
            this.place = place;
            new RetrieveData().execute();
        } else {
            view.displayErrorMessage("This field is empty");
        }
    }

    @Override
    public void displayAllMap(AddressResponse.ResultsBean item, List<AddressResponse.ResultsBean> listAddress) {
        item.setFormatted_address(view.getLabelAllPlaces());
        listAddress.add(item);
    }

    @Override
    public void populateCoordinateList(int lastItem, int position, List<LatLng> latLngList, AddressResponse.ResultsBean addresses, List<AddressResponse.ResultsBean> listAddress) {
        if (position != lastItem) {
            latLngList.clear();
            listAddress.clear();
            double lat = addresses.getGeometry().getLocation().getLat();
            double lng = addresses.getGeometry().getLocation().getLng();
            latLng = new LatLng(lat, lng);
            latLngList.add(latLng);
        } else {
            if(this.listAddress.get(lastItem).getGeometry()!=null){
                latLngList.clear();
                double lat = addresses.getGeometry().getLocation().getLat();
                double lng = addresses.getGeometry().getLocation().getLng();
                latLng = new LatLng(lat, lng);
                latLngList.add(latLng);
            }
        }

    }

    @Override
    public void goToMap(List<LatLng> latLngList, Context contexto, AddressResponse.ResultsBean addresses, int lastItem) {
        if (latLngList != null && latLngList.size() > 0) {
            Intent mapsIntent = new Intent(contexto, MapsActivity.class);
            mapsIntent.putExtra("place", addresses.getFormatted_address());
            mapsIntent.putParcelableArrayListExtra("latLngList", (ArrayList<? extends Parcelable>) latLngList);
            contexto.startActivity(mapsIntent);
        }
    }

    @Override
    public void getRetrofitConnection() {
        task = new RestApiKey();
        task.setServerResponseConnector(this);
    }

    public class RetrieveData extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.loadingResults();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            task.placesList(place, key);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            view.displayResults(listAddress);
        }
    }


    @Override
    public void onConnectionResult(int statusCode, Object responseData) {
        if (statusCode == RestApiKey.NO_CONNECTION_ERROR) {
            view.getErrorMessage(RestApiKey.NO_CONNECTION_ERROR);
            return;
        }

        if (statusCode == RestApiKey.GENERIC_RESPONSE_CODE_ERROR
                || statusCode == RestApiKey.NOT_FOUND
                || statusCode == RestApiKey.FORBIDDEN) {
            view.getErrorMessage(RestApiKey.GENERIC_RESPONSE_CODE_ERROR);
            return;
        }
        if (statusCode == 200) {
            if (responseData instanceof AddressResponse) {

                if(listAddress.size() > 0)
                    listAddress.clear();

                if(latLngList.size() > 0)
                    latLngList.clear();

                final AddressResponse addressResponse = (AddressResponse) responseData;


                for (final AddressResponse.ResultsBean resultPlace : addressResponse.getResults()) {
                    if(addressResponse.getResults().size() > 1) {
                        double lat = resultPlace.getGeometry().getLocation().getLat();
                        double lng = resultPlace.getGeometry().getLocation().getLng();
                        latLng = new LatLng(lat, lng);
                        latLngList.add(latLng);
                    }
                    listAddress.add(resultPlace);
                }

                if(listAddress != null && listAddress.size() > 0) {
                    if (listAddress.size() > 1) {
                        AddressResponse.ResultsBean item = new AddressResponse.ResultsBean();
                        displayAllMap(item,listAddress);
                    }
                    view.getMyAdapter(listAddress,latLngList);
                }

            }
        }

    }
}
