package com.linecorp.mapsappnatan.presenter;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.linecorp.mapsappnatan.data.PlacesDBHelper;
import com.linecorp.mapsappnatan.data.PlacesFields;

import java.util.List;

/**
 * Created by natanbrito on 18/06/2018.
 */

public class MapsPresenter implements PlacesContract.MapsPresenter {

    PlacesContract.MapsView view;
    String[] allPlaces;
    PlacesDBHelper dbHelper;
    private SQLiteDatabase mDatabase;


    public MapsPresenter(PlacesContract.MapsView view, PlacesDBHelper dbHelper) {
        this.view = view;
        this.dbHelper = dbHelper;
    }


    @Override
    public void savePlace(String place, List<LatLng> latLngList) {
        if(place != null && !place.isEmpty() && latLngList != null && latLngList.size() > 0){
            allPlaces = new String[latLngList.size()];
            for (int count = 0 ; count < latLngList.size() ; count++) {
                ContentValues cv = new ContentValues();
                cv.put(PlacesFields.PlacesEntry.PLACE_COLUMN,place);
                cv.put(PlacesFields.PlacesEntry.LAT_COLUMN, latLngList.get(count).latitude);
                cv.put(PlacesFields.PlacesEntry.LNG_COLUMN, latLngList.get(count).longitude);
                mDatabase.insert(PlacesFields.PlacesEntry.TABLE_NAME, null, cv);
                allPlaces[count] = place;
            }
        }
    }

    @Override
    public boolean isPlaceSaved(String place) {
        boolean isSaved = false;
        allPlaces = new String[]{place};
        if(allPlaces != null && allPlaces.length > 0){
            Cursor selectPlace = mDatabase.query(PlacesFields.PlacesEntry.TABLE_NAME,null, PlacesFields.PlacesEntry.PLACE_COLUMN+"=?",allPlaces,null,null,null);
            if(selectPlace.getCount() > 0)
                isSaved = true;
            else
                isSaved = false;
        }
        return isSaved;

    }

    @Override
    public int getPlaceToDelete() {
        int id = 0;
        String[] args = new String[]{PlacesFields.PlacesEntry._ID};
        Cursor cursor = mDatabase.query(PlacesFields.PlacesEntry.TABLE_NAME,args, PlacesFields.PlacesEntry.PLACE_COLUMN+"=?",allPlaces,null,null,null);
        if(cursor.moveToFirst()){
            id = cursor.getInt(cursor.getColumnIndex(PlacesFields.PlacesEntry._ID));
        }
        return id;
    }

    @Override
    public void removePlace() {
        int id = getPlaceToDelete();
        mDatabase.delete(PlacesFields.PlacesEntry.TABLE_NAME, PlacesFields.PlacesEntry._ID + " = "+id,null);
    }

    @Override
    public void startDb() {
        mDatabase = dbHelper.getWritableDatabase();
    }


    @Override
    public Cursor getSavedPlaces(){
        return mDatabase.query(PlacesFields.PlacesEntry.TABLE_NAME,null,null,null,null,null,null);
    }
}
