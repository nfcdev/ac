package com.linecorp.mapsappnatan.presenter;

import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import com.google.android.gms.maps.model.LatLng;
import com.linecorp.mapsappnatan.model.AddressResponse;

import java.util.List;

/**
 * Created by natanbrito on 17/06/2018.
 */

public interface PlacesContract {

    interface View {
        void displayErrorMessage(String s);
        void loadingResults();
        void displayResults(List<AddressResponse.ResultsBean> listAddress);
        void getMyAdapter(List<AddressResponse.ResultsBean> listAddress, List<LatLng> latLngList);

        String getLabelAllPlaces();

        void getErrorMessage(int genericResponseCodeError);
    }


    interface Presenter {
        void placesValidator(String place);
        void displayAllMap(AddressResponse.ResultsBean item, List<AddressResponse.ResultsBean> listAddress);
        void populateCoordinateList(int lastItem, int position, List<LatLng> latLngList, AddressResponse.ResultsBean addresses, List<AddressResponse.ResultsBean> listAddress);
        void goToMap(List<LatLng> latLngList, Context contexto, AddressResponse.ResultsBean addresses, int lastItem);
        void getRetrofitConnection();
    }

    interface MapsView {
    }

    interface MapsPresenter{
        void savePlace(String place, List<LatLng> latLngList);

        boolean isPlaceSaved(String place);
        int getPlaceToDelete();

        void removePlace();

        void startDb();

        Cursor getSavedPlaces();
    }
}
