package com.linecorp.mapsappnatan;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.linecorp.mapsappnatan.connections.RestApiKey;
import com.linecorp.mapsappnatan.model.AddressResponse;
import com.linecorp.mapsappnatan.presenter.PlacesContract;
import com.linecorp.mapsappnatan.presenter.PlacesPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements PlacesContract.View {

    @BindView(R.id.et_place)
    EditText etPlace;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.rv)
    RecyclerView recyclerView;

    @BindView(R.id.tv_no_results)
    TextView tvEmpty;

    ListPlacesAdapter adapter;

    String key;

    String place;

    List<String> placeList = new ArrayList<>();

    Context contexto;

    PlacesContract.Presenter placesPresenter;

    @BindView(R.id.cl)
    ConstraintLayout cl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        key = getResources().getString(R.string.google_maps_key);

        placesPresenter = new PlacesPresenter(this,key);

        contexto = this;

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.hasFixedSize();
        placesPresenter.getRetrofitConnection();
    }

    @OnClick(R.id.bt_search)
    public void SearchPlace(){
        place = etPlace.getText().toString();
        placesPresenter.placesValidator(place);
    }

    @Override
    public void displayErrorMessage(String s) {
        etPlace.setError(s);
    }

    @Override
    public void loadingResults(){
        tvEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    @Override
    public void displayResults(List<AddressResponse.ResultsBean> listAddress) {
        progressBar.setVisibility(View.GONE);
        if (listAddress.size() == 0)
            tvEmpty.setVisibility(View.VISIBLE);
        else {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(adapter);
            adapter.notifyItemInserted(listAddress.size() - 1);
        }
    }

    @Override
    public void getMyAdapter(final List<AddressResponse.ResultsBean> listAddress, final List<LatLng> latLngList){
        adapter = new ListPlacesAdapter(listAddress, this, new ListPlacesAdapter.OnClickListener() {
            @Override
            public void onItemClick(AddressResponse.ResultsBean addresses, int position) {
                recyclerView.setVisibility(View.GONE);
                int lastItem = listAddress.size()-1;
                placesPresenter.populateCoordinateList(lastItem, position, latLngList, addresses,listAddress);
                placesPresenter.goToMap(latLngList, contexto, addresses,lastItem);
            }
        });
    }

    @Override
    public String getLabelAllPlaces(){
        return getResources().getString(R.string.display_all);
    }

    @Override
    public void getErrorMessage(int code) {
        if(code == RestApiKey.NO_CONNECTION_ERROR)
            Snackbar.make(cl,getResources().getString(R.string.offline_device),Snackbar.LENGTH_INDEFINITE).show();
        if(code == RestApiKey.GENERIC_RESPONSE_CODE_ERROR)
            Snackbar.make(cl,getResources().getString(R.string.generic_error),Snackbar.LENGTH_INDEFINITE).show();
    }

}
