package com.linecorp.mapsappnatan;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.linecorp.mapsappnatan.model.AddressResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by natanbrito on 16/06/2018.
 */

public class ListPlacesAdapter extends RecyclerView.Adapter<ListPlacesAdapter.PlacesViewHolder> {

    List<AddressResponse.ResultsBean> resultList;
    Context context;

    private final OnClickListener onClickListener;


    public interface OnClickListener {
        void onItemClick(AddressResponse.ResultsBean addresses, int position);
    }

    public ListPlacesAdapter(List<AddressResponse.ResultsBean> resultList, Context context, OnClickListener onClickListener) {
        this.resultList = resultList;
        this.context = context;
        this.onClickListener = onClickListener;
    }


    @Override
    public ListPlacesAdapter.PlacesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.places_item_layout, parent, false);
        return new PlacesViewHolder(view);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_place) TextView tv_place;

        public PlacesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }


        public void bind(final AddressResponse.ResultsBean resultsBean, final OnClickListener clickListener) {
            tv_place.setText(resultsBean.getFormatted_address());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(resultsBean, getAdapterPosition());
                }
            });
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ListPlacesAdapter.PlacesViewHolder holder, int position) {
        holder.bind(resultList.get(position),onClickListener);
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }



}
